/**
 * Plugin to sort columns based on value
 * Will need `sortColumns` set as true in chart config to work
 */
AmCharts.addInitHandler(function(chart) {

  if (chart.sortColumns !== true)
    return;

  /**
   * Iterate through data
   */
  for (var i = 0; i < chart.dataProvider.length; i++) {

    // Collect all values for all graphs in this data point
    var row = chart.dataProvider[i];
    var values = [];
    for (var g = 0; g < chart.graphs.length; g++) {
      var graph = chart.graphs[g];
      values.push({
        "value": row[graph.valueField],
        "graph": graph
      });
    }

    // Sort by value
    values.sort(function(a, b) {
      return a.value - b.value;
    });
    
    // Apply `columnIndexField`
    for(var x = 0; x < values.length; x++) {
      var graph = values[x].graph;
      graph.columnIndexField = graph.valueField + "_index";
      row[graph.columnIndexField] = x;
    }
  }

}, ["serial"]);

var chart = AmCharts.makeChart("chartdiv-gender", {
  "type": "serial",
  "theme": "light",
  "sortColumns": true,
  "legend": {
    "horizontalGap": 2,
    "maxColumns": 1,
    "position": "right",
    "useGraphSettings": true,
    "markerSize": 10
  },
  "dataProvider": [{
    "year": 2003,
    "europe": 1.5,
    "namerica": 2.5,
  }, {
    "year": 2004,
    "europe": 2.6,
    "namerica": 2.7,
  }, {
    "year": 2005,
    "europe": 2.8,
    "namerica": 1.1,
  }],
  "valueAxes": [{
    "axisAlpha": 0.3,
    "gridAlpha": 0
  }],
  "graphs": [{
    "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
    "fillAlphas": 0.8,
    "labelText": "[[value]]",
    "lineAlpha": 0.1,
    "title": "Female",
    "type": "column",
    "fillColors": "#ff75da",
    "fillAlpha": 1,
    "valueField": "europe"
  }, {
    "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
    "fillAlphas": 0.8,
    "labelText": "[[value]]",
    "lineAlpha": 0.3,
    "title": "Male",
    "type": "column",
    "fillColors": "#00aeef",
    "fillAlpha": 1,
    "valueField": "namerica"
  }],
  "categoryField": "year",
  "categoryAxis": {
    "gridPosition": "start",
    "gridAlpha": 0,
    "position": "left"
  }

});